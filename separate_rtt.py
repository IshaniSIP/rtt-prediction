from fileinput import input

for line in input():
    dst, src, rtt, dt = line.strip().split()
    filename = "%s-%s-rtt.dat" % (dst, src)
    with open(filename, "a") as f:
        f.write(" ".join([rtt, dt]) + "\n")
