# Arjun Subramonian
# Jitter-Based Delay-Boundary Prediction of Wide-Area Networks

from rttpredictor import RTTPredictor
import datetime
import numpy as np

from jacobson import Jacobson

class JitterPredictor(RTTPredictor):
    def __init__(self, f, under_error = 1, over_error = -1, k = 2.6):
        RTTPredictor.__init__(self, under_error, over_error)
        self.count = 1
        self.hist = {}
        prevRTT = None
        self.prevTime = None
        for line in f:
            arr = line.strip().split()
            rtt = float(arr[0])
            time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S.%f")
            if self.count == 1:
                prevRTT = rtt
                self.prevTime = time
                self.count += 1
                continue
            td = (time - self.prevTime).total_seconds()
            if td not in self.hist:
                self.hist[td] = []
            self.hist[td].append(rtt - prevRTT)
            prevRTT = rtt
            self.prevTime = time
            self.count += 1
        for key in self.hist:
            arr = np.asarray(self.hist[key])
            # TODO: Original seemed wrong
            # in the paper: c_2(\tau) = sqrt(E[(X(t+tau)-X(t))^2])
            self.hist[key] = np.sqrt(np.mean(arr**2))
        self.k = k

        self.count = 1
        self.prevTime = None

        self.jacobson = Jacobson()

    def update_rto(self, rtt, time):
        if self.count != 1:
            td = (time - self.prevTime).total_seconds()
            if td in self.hist:
                self.rto = rtt - self.k * self.hist[td]
            else:
                self.rto = self.jacobson.update_rto(rtt, time)
        self.prevTime = time
        self.count += 1

    def process_loss(self, rtt, time):
        RTTPredictor.process_loss(self, rtt, time)
        if self.count != 1:
            td = (time - self.prevTime).total_seconds()
            if td in self.hist:
                self.rto = rtt + self.k * self.hist[td]
            else:
                self.rto = self.jacobson.update_rto(rtt, time)
        self.prevTime = time
        self.count += 1

