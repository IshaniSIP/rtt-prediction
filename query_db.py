from db import Session, Market, Country, City, Note
from sqlalchemy import func

session = Session()

for market_id, market in session.query(Market.id, Market.name):
    query = session.query(Note.note, Note.year, Note.month, Note.day,
                          City.name, Country.name).join(City).\
                          join(Country).filter(Country.market_id == market_id)
    for note, year, month, day, city, country in query.order_by(Note.year):
        print "%d-%d-%d-%s-%s-%s:" % (year, month, day, market, city, country),
        print note
        
