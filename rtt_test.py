#!/usr/bin/python

from machine_learning import MachineLearning
from model_based import ModelBased
from jitter import JitterPredictor
from jacobson import Jacobson
from weightedmedians import RWM

from sys import argv
import datetime

if __name__ == "__main__":
    predictors = [
        MachineLearning(), 
        ModelBased(), 
        JitterPredictor(open(argv[2], "r")), 
        RWM(),
        Jacobson(), 
    ]

    count = 0
    for line in open(argv[1], "r"):
        arr = line.strip().split()
        rtt = float(arr[0])
        time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S.%f")
        for p in predictors:
            p.receive_rtt(rtt, time)
        count += 1

    for p in predictors:
        print type(p).__name__, p.lost_packets*100.0/count,
        print p.error
    
