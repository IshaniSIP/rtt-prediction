# Daniel Alves
# Congestion Avoidance and Control

from rttpredictor import RTTPredictor

class Jacobson(RTTPredictor):
    def __init__(self, under_error=1, over_error=-1, alpha=1/8, beta=1/4):
        """Initializes the Jacobson algorithm (RFC6298)."""
        RTTPredictor.__init__(self, under_error, over_error)
        self.srtt = None
        self.rttvar = None
        self.k = 4
        self.b = beta
        self.a = alpha

    def update_rto(self, rtt, time):
        if self.srtt is None:
            self.srtt = rtt
            self.rttvar = rtt/2
            self.rto = self.srtt + max(self.granularity,
                                       self.k * self.rttvar)
        else:
            self.rttvar = (1 - self.b) * self.rttvar + (
                self.b * abs(self.srtt - rtt))
            self.srtt = (1 - self.a) * self.srtt + self.a * rtt
            self.rto = self.srtt + max(self.granularity,
                                       self.k * self.rttvar)
        return self.rto

    def process_loss(self, rtt, time):
        RTTPredictor.process_loss(self, rtt, time)
        self.rto = min(2*self.rto, 60*1000)
        return self.rto


