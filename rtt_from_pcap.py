import pyshark
from sys import argv
import netaddr
import datetime as dt

def to_mseconds(tv):
    return tv.total_seconds()*1000

target = 2154969872
clock = {}
for filename in argv[1:]:
    pcap = pyshark.FileCapture(filename)
    for pkt in pcap:
        src = int(netaddr.IPAddress(pkt.ip.src))
        dst = int(netaddr.IPAddress(pkt.ip.dst))
        if dst == target or src == target:
            # first packet of connection
            syn = int(pkt.tcp.flags_syn)
            ack = int(pkt.tcp.flags_ack)
            if dst == target:
                c = pkt.tcp.options_timestamp_tsval
                if c not in clock:
                    clock[c] = pkt.sniff_time
                if syn:
                    print -1
            elif src == target:
                c = pkt.tcp.options_timestamp_tsecr
                sent = clock[c]
                arrive = pkt.sniff_time
                print to_mseconds(arrive-sent)


