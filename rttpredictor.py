import datetime

class RTTPredictor(object):
    def __init__(self, under_error = 1, over_error = -1, min_rto = 100):
        """Initiates the algorithm's parameters with default values"""
        self.under_error = under_error
        self.over_error = over_error
        self.min_rto = min_rto
        self.rto = self.min_rto
        self.error = 0
        self.lost_packets = 0
        # timer granularity in milliseconds
        self.granularity = 100
        
    def receive_rtt(self, rtt, time):
        """Receive the next measured RTT and update internal parameters."""
        d = self.rto - rtt
        if d < 0:
            self.error += self.over_error * d
            self.process_loss(rtt, time)
        else:
            self.error += self.under_error * d
            self.update_rto(rtt, time)
            if self.rto < self.min_rto:
                self.rto = self.min_rto

    def update_rto(self, rtt, time):
        self.rto = rtt

    def process_loss(self, rtt, time):
        self.lost_packets += 1

