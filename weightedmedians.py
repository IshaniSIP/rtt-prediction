

from rttpredictor import RTTPredictor
from numpy import median
from numpy import mean
import numpy as np
from fileinput import input 

#This algroithm uses running weighted medians to predict future rtts
#The weights can be adjusted in the parameters gamma and delta 
#Ishani Karmarkar 
class RWM(RTTPredictor):
    def __init__(self, under_error = 1, over_error = -1, alpha = 1.0/8, beta = 1.0/4, gamma = .5, delta = 7.0/8):
        RTTPredictor.__init__(self, under_error, over_error)
        self.srtt = None
        self.rttvar = None
        self.k = 4
        self.a = alpha
        self.b = beta
        self.c = gamma
        self.d = delta
        self.tracker = False
        self.estimated = None
        self.data = [ ]

    #creates and returns list of the last five observations if possible
    def valuesList(self, rtt): 
        #implements Jacobson's algorithm for the first five (0 - 4) lines and updates self.rto
        if len(self.data) < 5:
            if self.srtt is None:
                self.srtt = rtt
                self.rttvar = rtt/2
                self.rto = self.srtt + max(self.granularity, self.k * self.rttvar)
            else:
                self.rttvar = (1 - self.b) * self.rttvar + (self.b * abs(self.srtt - rtt))
                self.srtt = (1 - self.a) * self.srtt + self.a * rtt
                self.rto = self.srtt + max(self.granularity, self.k * self.rttvar)
            self.data.append(rtt)
            return 
        #otherwise creates a list of the last five observations and the last expected rtt
        else: 
            self.data.append(self.rto)
            self.tracker = True

    #updates the value of self.rto using Jacobson's algorithm if there are not five previous observations, using running weighted medians otherwise
    def update_rto(self, rtt, time): 
        self.valuesList(rtt)

        if not self.tracker: 
            return
        else: 
            #this is the order in which the dataValues elements are rearranged once sorted
            order = np.argsort(self.data)
            data = sorted(self.data)
            #create a list with weights and sort the numbers array in the same order as the dataValues array
            weights = np.asarray([self.c, (self.d ** 0), (self.d ** 1), (self.d ** 2), (self.d ** 3), (self.d ** 4)])
            try:
                weights = weights[order]
            except IndexError:
                import pdb; pdb.set_trace()
            #creates a list with the cumulative weights
            cumulatives = [0, 0, 0, 0, 0, 0]
            mySum = 0 
            x = 0 
            while x < len(weights): 
                mySum += weights[x] 
                cumulatives[x] = mySum 
                x += 1
            #finds the value that represents half of the cumulative sum 
            index = (cumulatives[-1])/2
            #finds the index in dataValues/numbersthat corresponds to the weighted median
            value = 0 
            y = 0 
            while y < (len(weights) - 1): 
                if ((cumulatives[y +1] -  index) > 0 and (cumulatives[y] - index) < 0): 
                    data[y] = value 
                y += 1 
            #updates the value of self.rto
            self.rto = value
            self.data.append(rtt)
            self.data.pop(0)
            self.data.pop(-1)

    def process_loss(self, rtt, time):
        self.update_rto(rtt, time)
        RTTPredictor.process_loss(self, rtt, time)
        self.rto = min(2*self.rto, 60*1000)
