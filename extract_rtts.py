import pyshark
from sys import argv
import netaddr


class RTTCalculator(object):
    def __init__(self, src, dst):
        self.src = src
        self.dst = dst
        self.times = {}

    def process(self, pkt):
        src = int(netaddr.IPAddress(pkt.ip.src))
        dst = int(netaddr.IPAddress(pkt.ip.dst))
        #import pdb; pdb.set_trace()
        if dst == self.dst:
            try:
                ts = pkt.tcp.options_timestamp_tsval
            except AttributeError:
                return
            if ts not in self.times:
                self.times[ts] = pkt.sniff_time
        else:
            try:
                ts = pkt.tcp.options_timestamp_tsecr
            except AttributeError:
                return
            return (pkt.sniff_time - self.times[ts], self.times[ts])

def starting(pkt):
    if int(pkt.tcp.flags_syn) == 1 and int(pkt.tcp.flags_ack) == 0:
        return True
    return False
    
# this doesn't work in the general case, but for me sources and destinations
# will never exchange roles
def get_pair(src, dst, sources, pkt):
    if starting(pkt):
        sources.add(src)
    if src in sources:
        return "%d-%d" % (src, dst)
    if dst in sources:
        return "%d-%d" % (dst, src)

calculators = {}
sources = set()
for filename in argv[1:]:
    pcap = pyshark.FileCapture(filename)
    for pkt in pcap:
        if pkt.transport_layer is None:
            continue
        src = int(netaddr.IPAddress(pkt.ip.src))
        dst = int(netaddr.IPAddress(pkt.ip.dst))
        pair = get_pair(src, dst, sources, pkt)
        if pair is not None:
            if pair not in calculators:
                if starting(pkt):
                    calculators[pair] = RTTCalculator(src, dst)
                else:
                    continue
            ret = calculators[pair].process(pkt)
            if ret is not None:
                print src, dst, ret[0].total_seconds() * 1000, ret[1].isoformat()
